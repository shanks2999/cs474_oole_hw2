package shanks.customParser;

import com.github.javaparser.JavaParser;
import com.github.javaparser.ast.stmt.Statement;
import javafx.util.Pair;

import java.util.*;

public class TemplateClass {
    static String prefix = "shanks.customParser.TemplateClass.instrum(";


    public static Statement createInstrum(int lineNumber, String statementType, Pair<String, String> pair) {
        String myPairs = "new javafx.util.Pair<>(\"" + pair.getKey() + "\",String.valueOf(" + pair.getValue() + "))";
        String instrum = prefix + lineNumber + ",\"" + statementType + "\"," + myPairs + ");";
        return JavaParser.parseStatement(instrum);
    }


    public static Statement createInstrum(int lineNumber, String statementType, List<Pair<String, String>> pairList) {
        String myPairs = "";
        Set<String> mySet = new HashSet<String>();
        for (Pair pair : pairList) {
            if (!mySet.contains(pair.getValue())) {
                myPairs += "new javafx.util.Pair<>(\"" + pair.getKey() + "\",String.valueOf(" + pair.getValue() + ")),";
                mySet.add(pair.getValue().toString());
            }
        }
        myPairs = myPairs.substring(0, myPairs.length() - 1);
        String instrum = prefix + lineNumber + ",\"" + statementType + "\"," + myPairs + ");";
        return JavaParser.parseStatement(instrum);
    }

    public static void instrum(int lineNumber, String statementType, Pair<String, String> ... pair) {
        String myPairs = " %s : %s";

String instrumDisplay = " %d |  %s | ";
        List<String> pairConcat = new ArrayList<>();
        List<Object> instrumValues = new ArrayList<>();

        instrumValues.add(lineNumber);
        instrumValues.add(statementType);

        for (Pair<String, String> solePair : pair) {
            String temp = String.format(myPairs, solePair.getKey(), solePair.getValue() instanceof String ? "%s" : "%s ");
            pairConcat.add(temp);
            instrumValues.add(solePair.getValue());
        }
        instrumDisplay += String.join(" | ", pairConcat);

        System.out.println(String.format(instrumDisplay, instrumValues.toArray()));
    }

//    String tupleTemplate = " %s : %s"; // name (string value), type (%s, %d)
//    List<String> tupleConcat = new ArrayList<>();
//    List<Object> values = new ArrayList<>();
//
//// initializing params
//values.add(lineNumber);
//values.add(expression);
//
//for (Pair<String, Object> dataPoint : dataPoints) {
//        String temp = String.format(tupleTemplate, dataPoint.getKey(), dataPoint.getValue() instanceof Integer ? "%d" : "%s ");
//        tupleConcat.add(temp);
//        values.add(dataPoint.getValue());
//    }
//
//    String loggingTemplate = "L : %d || %s ||";
//    loggingTemplate += String.join(" || ", tupleConcat);
//
//System.out.println(String.format(loggingTemplate, values.toArray()));



//    public static String instrum(int lineNumber, String statementType, Pair<String, String> pair) {
//        String myPairs = "";
//        Set<String> mySet = new HashSet<String>();
//
//        myPairs += "new javafx.util.Pair(" + pair.getKey() + "," + pair.getValue() + "),";
//        mySet.add(pair.getValue().toString());
//
//        myPairs = myPairs.substring(0, myPairs.length() - 1);
////        System.out.println(myPairs);
//        String instrum = prefix + lineNumber + "," + statementType + "," + myPairs + ");";
////        System.out.println(instrum);
//        return instrum;
//    }

//
//    public static String instrum(int lineNumber, String statementType, List<Pair<String, String>> pairList) {
//        String myPairs = "";
//        Set<String> mySet = new HashSet<String>();
//        for (Pair pair : pairList) {
//            if (!mySet.contains(pair.getValue())) {
//                myPairs += "new javafx.util.Pair(" + pair.getKey() + "," + pair.getValue() + "),";
//                mySet.add(pair.getValue().toString());
//            }
//        }
//        myPairs = myPairs.substring(0, myPairs.length() - 1);
////        System.out.println(myPairs);
//        String instrum = prefix + lineNumber + "," + statementType + "," + myPairs + ");";
////        System.out.println(instrum);
//        return instrum;
//    }
}