package shanks.customParser;

import com.github.javaparser.JavaParser;
import com.github.javaparser.ast.CompilationUnit;
import com.github.javaparser.ast.Node;
import com.github.javaparser.ast.NodeList;
import com.github.javaparser.ast.PackageDeclaration;
import com.github.javaparser.ast.body.*;
import com.github.javaparser.ast.expr.*;
import com.github.javaparser.ast.stmt.*;
import com.github.javaparser.ast.visitor.ModifierVisitor;
import javafx.util.Pair;

import java.io.*;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.*;


public class Parser {
    static int appendIndex;
    static List<String> stackPath = new ArrayList<String>();

    public static void main(String args[]) throws Exception {
        List<File> trackedFiles = getAllJavaFiles();
        List<String> trackedSources = getAllJavaSources(trackedFiles);
        for (int i = 0; i < trackedFiles.size(); i++) {

            try {
                File javaFileOld = trackedFiles.get(i);
                System.out.println(javaFileOld.getCanonicalPath());

                CompilationUnit compilationUnit=null;
                try {
                    compilationUnit = JavaParser.parse(javaFileOld);
                    compilationUnit.accept(new MethodChangerVisitor(), null);
                }catch(Exception ex)
                {

                }
//        System.out.println(compilationUnit.toString());


                String canonicalPath = javaFileOld.getCanonicalPath().
                        substring(0, javaFileOld.getCanonicalPath().lastIndexOf("\\"));
                String oldFileName = javaFileOld.getName().substring(0, javaFileOld.getName().lastIndexOf("."));
                    javaFileOld.renameTo(new File(canonicalPath + "/" + oldFileName + "_Old.java"));
                    Writer writer = new BufferedWriter(new OutputStreamWriter(
                            new FileOutputStream(canonicalPath + "/" + oldFileName + ".java"), "utf-8"));
                    writer.write(compilationUnit.toString());
                    writer.close();
            } catch (Exception ex) {

            }

        }
    }


    private static class MethodChangerVisitor extends ModifierVisitor<Void> {

//        @Override
//        public Node visit(VariableDeclarator varDeclr, Void arg) {
//            pushStackPath(varDeclr.getName().asString());
//            super.visit(varDeclr, arg);
//            popStackPath();
//            return varDeclr;
//        }
//        @Override
//        public Node visit(ForeachStmt decl, Void arg) {
//            super.visit(decl, arg);
//            return decl;
//        }
//        @Override
//        public Node visit(ForStmt decl, Void arg) {
//            super.visit(decl, arg);
//            return decl;
//        }
//        @Override
//        public Node visit(WhileStmt decl, Void arg) {
//            super.visit(decl, arg);
//            return decl;
//        }

        @Override
        public Node visit(PackageDeclaration decl, Void arg) {
            pushStackPath(decl.getName().asString());
            super.visit(decl, arg);
            return decl;
        }

        @Override
        public Node visit(ClassOrInterfaceDeclaration decl, Void arg) {
            pushStackPath(decl.getName().asString());
            super.visit(decl, arg);
            popStackPath();
            return decl;
        }

        @Override
        public Node visit(MethodDeclaration decl, Void arg) {
            pushStackPath(decl.getName().asString());
            super.visit(decl, arg);
            List<Pair<String, String>> pairList = new ArrayList<Pair<String, String>>();
            NodeList<?> nn = decl.getNodeLists().get(0);
            String varName;
            for (Node n : nn) {
                varName = ((Parameter) n).getName().asString();
                pairList.add(new Pair<String, String>(readStackPath() + "." + varName, varName));
            }
            Statement statement = TemplateClass.createInstrum(decl.getRange().get().begin.line, "Method", pairList);
            decl.getBody().ifPresent(block -> block.addStatement(0, statement));

            popStackPath();
            return decl;
        }


        @Override
        public Node visit(BlockStmt decl, Void arg) {
            super.visit(decl, arg);
            List<Pair<String, String>> pairList;
            Pair<String, String> pair;
            int index;
            List<Statement> statementList = new ArrayList<>(decl.getStatements());
            for (Statement next : statementList) {
                if (next instanceof WhileStmt) {
                    Expression condition = ((WhileStmt) next).getCondition();
                    if (!(condition instanceof BooleanLiteralExpr)) {
                        pairList = new ArrayList<Pair<String, String>>();
                        findMethodsInCondition(condition, pairList);
//                    index = getAppendingLineNumber(statementList, condition);
                        if (pairList.size() > 0) {
                            Statement statement = TemplateClass.createInstrum(condition.getRange().get().begin.line, "While", pairList);
                            BlockStmt bb = ((BlockStmt) ((WhileStmt) next).getBody());
                            bb.addStatement(0, statement);
                        }
                    }
                }
                if (next instanceof ForStmt) {
                    Expression condition = ((ForStmt) next).getCompare().get();
                    pairList = new ArrayList<Pair<String, String>>();
                    String var = ((BinaryExpr) condition).getLeft().toString();
                    pair = new Pair<>(readStackPath() + "." + var, var);
                    Statement statement = TemplateClass.createInstrum(condition.getRange().get().begin.line, "FORFORFOR", pair);
//                    Statement statement = JavaParser.parseStatement(instrum);
                    BlockStmt bb = ((BlockStmt) ((ForStmt) next).getBody());
                    bb.addStatement(0, statement);
                } else if (next instanceof DoStmt) {
                    Expression condition = ((DoStmt) next).getCondition();
                    pairList = new ArrayList<Pair<String, String>>();
                    findMethodsInCondition(condition, pairList);
//                    index = getAppendingLineNumber(statementList, condition);
                    if (pairList.size() > 0) {
                        Statement statement = TemplateClass.createInstrum(condition.getRange().get().begin.line, "While", pairList);
                        BlockStmt bb = ((BlockStmt) ((DoStmt) next).getBody());
                        bb.addStatement(0, statement);
                    }
                } else if (next instanceof SwitchStmt) {
                    Expression condition = ((SwitchStmt) next).getSelector();
                    pair = new Pair<String, String>(readStackPath() + "." + condition.toString(), condition.toString());
                    Statement statement = TemplateClass.createInstrum(condition.getRange().get().begin.line, "Switch", pair);
//                    Statement statement = JavaParser.parseStatement(instrum);
                    changeStuff(condition, statement, "switch");
                } else if (next instanceof IfStmt) {
                    Expression condition = ((IfStmt) next).getCondition();
                    pairList = new ArrayList<Pair<String, String>>();
                    findMethodsInCondition(condition, pairList);
                    if (pairList.size() > 0) {
                        Statement statement = TemplateClass.createInstrum(condition.getRange().get().begin.line, "If", pairList);
                        BlockStmt bb = ((BlockStmt) ((IfStmt) next).getThenStmt());
                        bb.addStatement(0, statement);
                    }
                } else if (next instanceof ExpressionStmt) {
                    try {
                        Expression condition = ((ExpressionStmt) next).getExpression();
                        if (condition instanceof VariableDeclarationExpr) {
                            String vName = ((VariableDeclarationExpr) condition).getVariables().get(0).getName().asString();
                            pair = new Pair<String, String>(readStackPath() + "." + vName, vName);
                            Statement statement = TemplateClass.createInstrum(condition.getRange().get().begin.line, "Assign", pair);
//                        Statement statement = JavaParser.parseStatement(instrum);
                            changeStuff(condition, statement, "=");
                        }
                    }catch(Exception ex)
                    {

                    }
                }
            }

            return decl;
        }

        private void changeStuff(Expression condition, Statement statement, String keyWord) {
            Optional<BlockStmt> parentBlock = condition.getAncestorOfType(BlockStmt.class);
            NodeList<Statement> stmnts = parentBlock.get().getStatements();
            for (int i = 0; i < stmnts.size(); i++) {
                if (stmnts.get(i).toString().contains(condition.toString()) &&
                        stmnts.get(i).toString().contains(keyWord)) {
                    appendIndex = i;
                    break;
                }
            }
            parentBlock.ifPresent(block -> block.addStatement(appendIndex + 1, statement));
        }


//        private int getAppendingLineNumber(List<Statement> stmnts, Expression condition) {
//            int index = 0;
//            for (int i = 0; i < stmnts.size(); i++) {
//                if (stmnts.get(i).toString().contains(condition.toString())) {
//                    index = i;
//                    break;
//                }
//            }
//            return index;
//        }

        private void findMethodsInCondition(Expression condition, List<Pair<String, String>> pairList) {
            try {
                String var = "";
                if (condition instanceof BinaryExpr) {
                    BinaryExpr expr = (BinaryExpr) condition;
                    //Explore left part
//                if (expr.getLeft() instanceof EnclosedExpr) {
//                    Expression en = ((EnclosedExpr) ((Expression) expr.getLeft())).getInner();
//                    if (en instanceof AssignExpr)
//                        var = ((AssignExpr) en).getTarget().toString();
//                    pairList.add(new Pair<String, String>(readStackPath() + "." + var, var));
//                }
                    //Explore left part
//                if (expr.getLeft() instanceof FieldAccessExpr) {
//                    Expression en = ((FieldAccessExpr) ((Expression) expr.getLeft()));
//                    var = ((FieldAccessExpr) en).getTokenRange().toString();
//                    pairList.add(new Pair<String, String>(readStackPath() + "." + var, var));
//                }
                    if (expr.getLeft() instanceof BinaryExpr)
                        findMethodsInCondition(expr.getLeft(), pairList);
                    else if (expr.getLeft() instanceof NameExpr) {
                        pairList.add(new Pair<String, String>(readStackPath() + "." + expr.getLeft().toString(), expr.getLeft().toString()));
                    }

                    //Explore right part
                    if (expr.getRight() instanceof BinaryExpr)
                        findMethodsInCondition(expr.getRight(), pairList);
//                if (expr.getRight() instanceof EnclosedExpr) {
//                    Expression en = ((EnclosedExpr) ((Expression) expr.getRight())).getInner();
//                    if (en instanceof AssignExpr)
//                        var = ((AssignExpr) en).getTarget().toString();
//                    pairList.add(new Pair<String, String>(readStackPath() + "." + var, var));
//                }
//                if (expr.getRight() instanceof FieldAccessExpr) {
//                    Expression en = ((FieldAccessExpr) ((Expression) expr.getLeft()));
//                    var = ((FieldAccessExpr) en).getNameAsString();
//                    pairList.add(new Pair<String, String>(readStackPath() + "." + var, var));
//                }
                    else if (expr.getRight() instanceof NameExpr) {
                        pairList.add(new Pair<String, String>(readStackPath() + "." + expr.getRight().toString(), expr.getRight().toString()));
                    }

                }
            }catch(Exception ex){
                return;
            }
        }
    }

    ;


    public static void pushStackPath(String push) {
        stackPath.add(push);
    }

    public static void popStackPath() {
        stackPath.remove(stackPath.size() - 1);
    }

    public static String readStackPath() {
        String concat = "";
        for (int i = 0; i < stackPath.size(); i++)
            concat += stackPath.get(i) + ".";
        return concat.substring(0, concat.length() - 1);
    }

    public static List<String> getAllJavaSources(List<File> trackedFiles) throws IOException {
        List<String> trackedSources = new ArrayList<String>();
        for (File file : trackedFiles) {
            String contents = new String(Files.readAllBytes(Paths.get(file.getCanonicalPath())));
            trackedSources.add(contents);
        }
        return trackedSources;
    }

    public static List<File> getAllJavaFiles() throws IOException {
        File src = new File("./src");
        String absolutePath = src.getCanonicalPath();
        List<File> trackedFiles = fileTracker(absolutePath, new ArrayList<File>());
        return trackedFiles;
    }

    public static List<File> fileTracker(String path, List<File> trackedFiles) throws IOException {
        File trackFile = new File(path);
        for (File file : trackFile.listFiles()) {
            if (file.toString().endsWith(".java") && !file.toString().endsWith("Parser.java") && !file.toString().endsWith("TemplateClass.java"))
                trackedFiles.add(file);
            else if (file.isDirectory())
                fileTracker(path + "/" + file.getName(), trackedFiles);
        }
        return trackedFiles;
    }
}