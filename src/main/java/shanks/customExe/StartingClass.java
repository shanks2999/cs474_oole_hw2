package shanks.customExe;

import gcardone.junidecode.Junidecode;

public class StartingClass
{
    public static void main(String args[])
    {
        int myInt1 = 10;
        int myInt2 = 20;

        if(myInt2 > myInt1)
        {
            System.out.println("Hello World !");
        }
        while(myInt1 < myInt2)
        {
            String s = Junidecode.unidecode("\"⠏⠗⠑⠍⠊⠑⠗\"");
            System.out.println(s);
            myInt1++;
        }

    }

    public static void myMethod(double myDouble1,double myDouble2)
    {
        System.out.println("Inside myMethod");
    }
}
