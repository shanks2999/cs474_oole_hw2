package shanks.customTest;
import gcardone.junidecode.Junidecode;
import org.junit.Assert;
import org.junit.Test;

public class JUnitTest
{
    @Test
    public void test_1()
    {
        //Decoding Diatrics
        String str = Junidecode.unidecode("résumé");
        Assert.assertTrue(str.equals("resume"));
    }

    @Test
    public void test_2()
    {
        //Decoding Hindi Language
        Assert.assertTrue(Junidecode.unidecode("देवनागरी").equals("devnaagrii"));
    }
}
