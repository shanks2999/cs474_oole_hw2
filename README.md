# README #

# OOLE Assignment Homework 2 #


**Original Repository Source:** Junidecode from GitHub

- **Link to Original Repo:** https://github.com/gcardone/junidecode

**Description:** It�s a library which is used to convert Unicode to 7-Bit ASCII. Helpful in removing diatrics from a string, convert Latin accents and other foreign language to their English representations.

**Junit Tests:** The application came with 2 of its own test cases, nevertheless I created my 2 custom test cases within `shanks.customTest.JUnitTest.java` file inside the `test<java` package.

**Main Method:** I created a custom main method within `shanks.customParser.Parser.java` inside `main<java` package which is my AST parser application. The same package contains the `TemplateClass.java` with instrum method. I have tested the code on the `shanks.customExe.StartingClass.java` and is working perfectly.

**Build Configurations:**

- **Gradle:** Created gradle script named `build.gradle` in project folder. Cleaned it using command `gradle clean` and built it using `gradle build`

	- **Reference:**  https://www.tutorialspoint.com/gradle/gradle_dependency_management.htm

	- **Snap:** ![Snapshot for Gradle build script](/snaps/gradle_build.jpg)

**Procedure:**

- I forked the repository created and copied it to my system with command `git clone <URI>`
- Cloned project from github and pasted it in my forked repo and opened that project with IntelliJ.
- Created classes for Parser and Template.
- Built Gradle scripts. (Haven't created SBT script)
- Created parser code which is modifying all files except the `Parser.java` and `TemplateClass.java`.
- Added changes and pushed into the BitBucket with command `git add -A`
- Committed code with command `git commit -m <�message�>`
- Pushed it using command `git push origin master`
- DONE !
	
**Limitation:**

- As my project is not a standalone application, I have to build seperate main class for it i.e. `StartingClass.java` for testing AST
- The Parser API chosen is Java Parser and not Eclipse Parser as many documentaion available are not recent ones. Whereas documentation for `Java Parser` is provided for all the versions
	- **Reference:**  http://www.javadoc.io/doc/com.github.javaparser/javaparser-core/3.3.4
-All Run and Build are done using IntelliJ IDE

**PS:** 

- Completed entire assignment but haven't attempted the Bonus points part of the assignment as this alone took more than 30+ hours of effort. First attempting Eclipse parser then after numerous failed attempts switched to Java Parser.